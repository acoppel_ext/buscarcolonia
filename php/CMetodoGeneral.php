<?php

define('RUTA_LOGX',					'/sysx/progs/log/consultas');
class CMetodoGeneral
{
	function __construct()
	{
		date_default_timezone_set('America/Mazatlan');
	}
	function __destruct()
	{
	}
	
	var $cnxDb;
	
	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX .  '_' . date("d") . "txt"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}
    public function getRealIP() 
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
           
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
       
        return $_SERVER['REMOTE_ADDR'];
    }
    
	public function leerXmlServ($archivo, $idElem)
	{
		$res = "";
		if( file_exists($archivo) )
		{
			$datosXml = simplexml_load_file($archivo);
			if($datosXml)
			{
				foreach($datosXml->Servidor as $elem)
				{
					if( $elem->id == $idElem )
						$res = $elem;
				}
			}
			else
			{
				$this->grabarLogx("formato XML invalido");
				$res = false; 
			}
		} 
		else
		{
			$res = "El archivo " . $archivo . " no existe";
			$this->grabarLogx($res);
			$res = false; 
		}
		return $res;
	}
	
	public function obtenerConexionBdPostgres($DireccionIp, $BaseDato, $UsrBaseDato, $PasswdBaseDato)
	{
		global $cnxDb;
		if( pg_connection_status($cnxDb) != PGSQL_CONNECTION_OK )
		{
			$cadConexion = "host=" . $DireccionIp . " dbname=" . $BaseDato . " user=" . $UsrBaseDato . " password=" . $PasswdBaseDato;
			$cnxDb = pg_pconnect($cadConexion);
			if( $cnxDb )
				$this->grabarLogx('[obtenerConexionBdPostgres] Conexion establecida');
		}
		
		return $cnxDb;
	}
}

?> 
