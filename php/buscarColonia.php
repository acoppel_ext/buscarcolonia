<?php
	include("CMetodoGeneral.php");
	//include("JSON.php");
	date_default_timezone_set('America/Mazatlan');
	$configaraciones = simplexml_load_file("../../conf/webconfig.xml");
	$ipServidor = $configaraciones->Spa;
	$sUsuario = $configaraciones->Usuario;
	$sBasedeDatos = $configaraciones->Basededatos;
	$sPassword = $configaraciones->Password;

	define("IP_SERV",			$ipServidor);
	define("USR_BD",			$sUsuario);
	define("BD__",				$sBasedeDatos);
	define("PWD_BD",			$sPassword);


	define('OK___',					 1);
	define('ERR__',					-1);
	$cnxBd = null;
	$arrErr = array();
	$arrResp = array();
	$objGn = new CMetodoGeneral();
	//$json = new Services_JSON();
	$cCodPostal = "";
	$cNomColonia = "";
	$iEstado = 0;
	$iCiudad = 0;
	$iMunicipio = 0;
	$iOpcion = 0;
	$bFlag = false;
	if(isset($_POST['opcion']))
	{
		$iOpcion = $_POST['opcion'];
		switch ($iOpcion)
		{
			case 1: //Catalogo de estados
				$bFlag = true;
				break;
			case 2:	//Catalogo de ciudades
			case 3:	//Catalogo de municipios
				if(isset($_POST['idestado']))
				{
					$iEstado =  $_POST['idestado'];
					$bFlag = true;
				}
				else
				{
					$arrResp['estatus'] = ERR__;
					$arrResp['descripcion'] =  'Parametros incorrectos para consulta de ciudades o municipios';
				}
				break;
			case 4:	//consulta por codigo postal y nombre de colonia
				if(isset($_POST['codpostal']) )
				{
					if(isset($_POST['nomcolonia']))
						$cNomColonia = $_POST['nomcolonia'];

					$cCodPostal = $_POST['codpostal'];
					$bFlag = true;
				}
				else
				{
					$arrResp['estatus'] = ERR__;
					$arrResp['descripcion'] =  'Parametros incorrectos para consulta por código postal';
				}
				break;
			case 5:	//consulta por estado, ciudad, municipio y nombre colonia
				if(isset($_POST['idestado']) && isset($_POST['idciudad']) && isset($_POST['idmunicipio']) )
				{
					if(isset($_POST['nomcolonia']))
						$cNomColonia = $_POST['nomcolonia'];

					$iEstado = $_POST['idestado'];
					$iCiudad = $_POST['idciudad'];
					$iMunicipio = $_POST['idmunicipio'];
					$cNomColonia = $_POST['nomcolonia'];
					$bFlag = true;
				}
				else
				{
					$arrResp['estatus'] = ERR__;
					$arrResp['descripcion'] =  'Parametros incorrectos para consulta por estado, ciudad, municipio';
				}
			case 6://Fecha Servidor
				$bFlag = true;
				break;
			default:
				$arrResp['estatus'] = ERR__;
				$arrResp['descripcion'] =  'Parametros incorrectos';
				break;
		}

		if($bFlag)
		{
			try
			{
				$cnxBd =  new PDO( "pgsql:host=".IP_SERV.";port=5432;dbname=".BD__, USR_BD, PWD_BD);
				if($cnxBd)
				{
					switch ($iOpcion)
					{
						case 1: //Catalogo de estados
							$arrResp = 	consultarParteDomicilio(1,0);
							break;
						case 2:	//Catalogo de ciudades
							$arrResp = 	consultarParteDomicilio(2,$iEstado);
							break;
						case 3:	//Catalogo de municipios
							$arrResp = 	consultarParteDomicilio(3,$iEstado);
							break;
						case 4:	//consulta por codigo postal y nombre de colonia
							$arrResp = consultarColonia($cCodPostal, $iEstado, $iCiudad, $iMunicipio, $cNomColonia);
							break;
						case 5:	//consulta por estado, ciudad, municipio y nombre colonia
							$arrResp = consultarColonia($cCodPostal, $iEstado, $iCiudad, $iMunicipio, $cNomColonia);
							break;
						case 6:
							$arrResp = obtenerFechaServidor();
						default:
							break;
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$arrResp['estatus'] = ERR__;
					$arrResp['descripcion'] = "Error en conexión a la base de datos";
				}
				$cnxBd = null;
			}
			catch(PDOException $ex)
			{
				echo $ex->getMessage();
				die();
			}
		}
	}
	else
	{
		$arrResp['estatus'] = ERR__;
		$arrResp['descripcion'] =  'Parametros incorrectos';
	}
	echo json_encode($arrResp);

	function consultarColonia($cCodPostal, $iEstado, $iCiudad, $iMunicipio, $cNomColonia)
	{
		$iContador = 0;
		global $arrErr;
		global $cnxBd;
		global $objGn;
		$arrDatos = array();
		$cSql = "";
		$cSql = "select CodigoPostal,NombreColonia,NombreCiudad,NombreMunicipio,NombreEstado,idColonia,idCiudad,idMunicipio,idEstado " .
				"from fnConsultarColonia('" . $cCodPostal . "', ".$iEstado .", ". $iMunicipio .", ". $iCiudad .", '" . utf8_decode($cNomColonia) ."')";
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $cSql);

		$resulSet = $cnxBd->query($cSql);
		if($resulSet)
		{
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
			foreach($resulSet as $reg)
			{		
				$reg['id'] = $iContador;		
				$reg['nombrecolonia'] = utf8_encode($reg['nombrecolonia']);
				$reg['nombreciudad'] = utf8_encode($reg['nombreciudad']);
				$reg['nombremunicipio'] = utf8_encode($reg['nombremunicipio']);
				$reg['nombreestado'] = utf8_encode($reg['nombreestado']);
				$arrDatos['registros'][]=array_map('trim', $reg);
				$iContador++;
			}
		}
		else
		{
			$arrErr = $cnxBd->errorInfo();
			$objGn->grabarLogx( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			$arrDatos['estatus'] = ERR__;
			$arrDatos['descripcion'] = "Error en consulta";
		}
		return $arrDatos;
	}
	function consultarParteDomicilio( $iIdParte, $iEstado )
	{
		global $arrErr;
		global $cnxBd;
		global $objGn;
		$arrDatos = array();
		$cSql = "";
		$cSql = "select idCatalogo, Descripcion from fnConsultarCatEntidadDomicilio(" . $iIdParte . "::SMALLINT, ". $iEstado .");";
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $cSql);
		$resulSet = $cnxBd->query($cSql);
		if($resulSet)
		{
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
			foreach($resulSet as $reg)
			{	
				$reg['idcatalogo'] = utf8_encode($reg['idcatalogo']);
				$reg['descripcion'] = utf8_encode($reg['descripcion']);			
				$arrDatos['registros'][]=array_map('trim', $reg);
			}
		}
		else
		{
			$arrErr = $cnxBd->errorInfo();
			$objGn->grabarLogx( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			$arrDatos['estatus'] = ERR__;
			$arrDatos['descripcion'] = "Error en consulta";
		}
		return $arrDatos;
	}
	/**
	* devuelve la fecha del servidor
	*@return array $arrDatos
	*/
	function obtenerFechaServidor()
	{
		$arrDatos = array();
		setlocale(LC_TIME, 'es_MX.UTF-8	');
		$arrDatos["fecha"] = strtoupper(strftime("%A, %d de %B de %Y"));
		return $arrDatos;
		
	}
?>