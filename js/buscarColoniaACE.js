var arrDatosColonia = {};
var callback = function () { };

function iniDivBuscarColonia(functionCallBack) {
    //La funcion que se recibe por parametro se hace global para llamarla de cualquier metodo
    callback = functionCallBack;
    $('#divBuscarColonia').hide(0);
    $('#divBuscarColonia').dialog
        ({
            autoOpen: false,
            resizable: false,
            width: '80%',
            height: 'auto',
            modal: true,
            closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
            position: { my: "center", at: "center", of: $("body"), within: $("body") }//Posicionar el dialog en el centro
        });
    //Ocultar la barra de titulo del Dialogo
    $("#divBuscarColonia").siblings('div.ui-dialog-titlebar').remove();
    //Dar estilo a los botones
    $('#btnBuscarColonia,#btnCancelarBuscarColonia,#btnSeleccionarColonia').button();
    //Asignar eventos clicks
    $('#btnBuscarColonia').click(function () { buscarColonias(); });
    $('#btnSeleccionarColonia').click(function () { seleccionarReg(); });

    $("#btnCancelarBuscarColonia").click(function () {
        $('#divBuscarColonia').dialog("destroy");
        if (typeof callback == 'function') {
            callback();
        }
    });

    $("#cboEstado").change(function () {
        if ($("#cboEstado").val() != "-1") {
            consultarCiudad();
            consultarMunicipio();
        }
    });
    $("#idtxtCodPostal").keypress(function (ev) {
        if (ev.keyCode == 13) {
            buscarColonias();
        }
    });

    consultarFechaServidor();
    iniGridColonias();
    consultarEstados();
    $('#divBuscarColonia').dialog("open");
}

function iniGridColonias() {
    //anchoGrid = screen.width - 25;
    anchoGrid = 955;
    //$('#gridColonias').jqGrid('GridUnload');
    $('#gridColonias').jqGrid({
        width: anchoGrid,
        //height: 350,
        height: 220,
        dataType: 'local',
        colNames: ['ID', 'COD. POSTAL', 'COLONIA', 'POBLACIÓN', 'MUNICIPIO', 'ESTADO', "", "", "", ""],
        colModel: [{ name: 'id', index: 'id', jsonmap: 'id', width: 10, sortable: false, align: 'center', hidden: true },
        { name: 'codigopostal', index: 'codigopostal', jsonmap: 'codigopostal', width: 110, sortable: false, align: 'center' },
        { name: 'nombrecolonia', index: 'nombrecolonia', jsonmap: 'nombrecolonia', width: 300, sortable: false, align: 'left' },
        { name: 'nombreciudad', index: 'nombreciudad', jsonmap: 'nombreciudad', width: 180, sortable: false, align: 'left' },
        { name: 'nombremunicipio', index: 'nombremunicipio', jsonmap: 'nombremunicipio', width: 160, sortable: false, align: 'left' },
        { name: 'nombreestado', index: 'nombreestado', jsonmap: 'nombreestado', width: 160, sortable: false, align: 'left' },
        { name: 'idcolonia', index: 'idcolonia', jsonmap: 'idcolonia', width: 10, sortable: false, align: 'left', hidden: true },
        { name: 'idciudad', index: 'idciudad', jsonmap: 'idciudad', width: 10, sortable: false, align: 'left', hidden: true },
        { name: 'idmunicipio', index: 'idmunicipio', jsonmap: 'idmunicipio', width: 10, sortable: false, align: 'left', hidden: true },
        { name: 'idestado', index: 'idestado', jsonmap: 'idestado', width: 10, sortable: false, align: 'left', hidden: true }
        ],
        caption: '...',
        //Indicamos que se cargara solamente una vez la informacion.
        loadonce: true,
        //Se desactiva el boton para mostrar u ocultar el grid.
        hidegrid: false,
        //Se indica la forma en que se reajustaran las columnas.
        shrinkToFit: false,
        //Mostrar u Ocultar el elemento incial y final mostrado de el total
        viewrecords: true,
        //indica si permite seleccionar varios renglones a la vez
        multiselect: false,
        //El modo de ordenamiento - en este caso, descendente
        //sortorder: "desc",
        //indica la cantidad de registros que se deben de mostrar por pagina.
        //rowNum:-1,
        //Indica las opciones del dropdownlist, opciones de seleccion de cuantos registros por pagina
        //rowList:[10],
        //indica el contenedor donde estara el paginador.
        //pager: '#jqPager',
        //ExpandColClick: true,
        loadui: "enable",
        //ignorar Mayusculas al realizar busquedas en grid
        ignoreCase: true,
        ondblClickRow: function (rowid, iRow, iCol, e) {
            arrDatosColonia = $("#gridColonias").jqGrid('getRowData', rowid);
            $('#divBuscarColonia').dialog("destroy");
            if (typeof callback == 'function') {
                callback();
            }
        }
    });
    jQuery("#gridColonias").jqGrid(
        'bindKeys', {
            "onEnter": function (rowid) {
                arrDatosColonia = $("#gridColonias").jqGrid('getRowData', rowid);
                if (typeof callback == 'function') {
                    callback();
                }
                $('#divBuscarColonia').dialog("destroy");
            }
        });
    /*
        jQuery("#gridColonias").jqGrid(
            'filterToolbar', {
                stringResult:true,
                searchOnEnter:true,
                defaultSearch:"cn"
            });
    */
}

function buscarColonias() {
    sCodPostal = $('#idtxtCodPostal').val();
    sNomColonia = $('#idtxtColonia').val();
    iOpcion = 0;
    bContinuar = false;
    if (sCodPostal != '') {
        iOpcion = 4;
        parametros = { opcion: iOpcion, codpostal: sCodPostal, nomcolonia: sNomColonia };
        bContinuar = true;
    }
    else {
        iOpcion = 5;
        if ($("#cboEstado").val() != "-1" && $("#cboCiudad").val() != "-1" && $("#cboMunicipio").val() != "-1") {
            parametros = { opcion: iOpcion, idestado: $("#cboEstado").val(), idciudad: $("#cboCiudad").val(), idmunicipio: $("#cboMunicipio").val(), nomcolonia: $("#idtxtColonia").val() };
            bContinuar = true;
        }
    }
    if (bContinuar) {
        $("#gridColonias").jqGrid('clearGridData');
        $.ajax({
            url: '../buscarColonia/php/buscarColonia.php',
            type: 'POST',
            datatype: 'json',
            async: true,
            cache: false,
            data: parametros,
            success: function (data) {
                //$("#idCargando").hide();
                var datos = eval('(' + data + ')');
                switch (datos.estatus) {
                    case 1:
                        $('#gridColonias').jqGrid('addRowData', 'id', datos.registros);
                        $('#gridColonias').setCaption((sCodPostal != '') ? 'CÓDIGO POSTAL CONSULTADO: ' + sCodPostal : '...');
                        break;
                    default:
                        alert(datos.descEstatus);
                        break;
                }
            },
            beforeSend: function () {
                $('#gridColonias').setCaption("CARGANDO...");
                //$("#idCargando").show();
            },
            error: function (a, b, c) {
                $('#gridColonias').setCaption("...");
                console.log("Error - 1: " + JSON.stringify(a));
                console.log("Error - 2: " + JSON.stringify(b));
                console.log("Error - 3: " + JSON.stringify(c));
            }
        });
    }
    else {
        alert("Por favór capture un código postal o seleccione estado, cuidad y delegación");
    }
}

function seleccionarReg() {
    var ren = $("#gridColonias").jqGrid('getGridParam', 'selrow');
    if (ren) {
        arrDatosColonia = $("#gridColonias").jqGrid('getRowData', ren);
        $('#divBuscarColonia').dialog("destroy");
        if (typeof callback == 'function') {
            callback();
        }
    }
    else {
        alert("Debe seleccionar un registro");
    }
    return false;
}

function consultarEstados() {
    $.ajax({
        url: '../buscarColonia/php/buscarColonia.php',
        type: 'POST',
        datatype: 'json',
        async: false,
        cache: false,
        data: { opcion: 1 },
        success: function (data) {

            var datos = eval('(' + data + ')');
            switch (datos.estatus) {
                case 1:
                    sHtml = "<option value = -1>Seleccione una Opción</option>";
                    for (var i = 0; i < datos.registros.length; i++) {
                        sHtml += ("<option value =" + datos.registros[i].idcatalogo + ">" + datos.registros[i].descripcion.toUpperCase() + "</option>");
                    }
                    $('#cboEstado').html(sHtml);
                    break;
                default:
                    alert(datos.descEstatus);
                    break;
            }
            //desbloquearUI();
        },
        beforeSend: function () {
            //bloquearUI("Consultado datos del trabajador, por favór espere..");
        },
        error: function (a, b, c) {
            //desbloquearUI();
            console.log("Error - 1: " + JSON.stringify(a));
            console.log("Error - 2: " + JSON.stringify(b));
            console.log("Error - 3: " + JSON.stringify(c));
        }
    });
}

function consultarCiudad() {
    iIdEstado = $("#cboEstado").val();
    $.ajax({
        url: '../buscarColonia/php/buscarColonia.php',
        type: 'POST',
        datatype: 'json',
        async: true,
        cache: false,
        data: { opcion: 2, idestado: iIdEstado },
        success: function (data) {

            var datos = eval('(' + data + ')');
            console.log("cboCiudad");
            console.log(datos);
            switch (datos.estatus) {
                case 1:
                    sHtml = "<option value = -1>Seleccione una Opción</option>";
                    for (var i = 0; i < datos.registros.length; i++) {
                        sHtml += ("<option value =" + datos.registros[i].idcatalogo + ">" + datos.registros[i].descripcion.toUpperCase() + "</option>");
                    }
                    $('#cboCiudad').empty();
                    $('#cboCiudad').html(sHtml);

                    break;
                default:
                    alert(datos.descEstatus);
                    break;
            }
            //desbloquearUI();
        },
        beforeSend: function () {
            //bloquearUI("Consultado datos del trabajador, por favór espere..");
        },
        error: function (a, b, c) {
            //desbloquearUI();
            console.log("Error - 1: " + JSON.stringify(a));
            console.log("Error - 2: " + JSON.stringify(b));
            console.log("Error - 3: " + JSON.stringify(c));
        }
    });
}

function consultarMunicipio() {
    iIdEstado = $("#cboEstado").val();

    $.ajax({
        url: '../buscarColonia/php/buscarColonia.php',
        type: 'POST',
        datatype: 'json',
        async: true,
        cache: false,
        data: { opcion: 3, idestado: iIdEstado },
        success: function (data) {

            var datos = eval('(' + data + ')');
            console.log("cboMunicipio");
            console.log(datos);
            switch (datos.estatus) {
                case 1:
                    sHtml = "<option value = -1>Seleccione una Opción</option>";
                    for (var i = 0; i < datos.registros.length; i++) {
                        sHtml += ("<option value =" + datos.registros[i].idcatalogo + ">" + datos.registros[i].descripcion.toUpperCase() + "</option>");
                    }

                    $('#cboMunicipio').empty();
                    $('#cboMunicipio').html(sHtml);

                    break;
                default:
                    alert(datos.descEstatus);
                    break;
            }
            //desbloquearUI();
        },
        beforeSend: function () {
            //bloquearUI("Consultado datos del trabajador, por favór espere..");
        },
        error: function (a, b, c) {
            //desbloquearUI();
            console.log("Error - 1: " + JSON.stringify(a));
            console.log("Error - 2: " + JSON.stringify(b));
            console.log("Error - 3: " + JSON.stringify(c));
        }
    });
}

function consultarFechaServidor() {
    $.ajax({
        url: '../buscarColonia/php/buscarColonia.php',
        type: 'POST',
        datatype: 'json',
        async: false,
        cache: false,
        data: { opcion: 6 },
        success: function (data) {
            var datos = eval('(' + data + ')');
            $("#fechaActual").text(datos.fecha.toUpperCase());
        },
        beforeSend: function () {
        },
        error: function (a, b, c) {
            console.log("Error - 1: " + JSON.stringify(a));
            console.log("Error - 2: " + JSON.stringify(b));
            console.log("Error - 3: " + JSON.stringify(c));
        }
    });
}